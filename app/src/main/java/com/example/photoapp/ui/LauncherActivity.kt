package com.example.photoapp.com.example.photoapp.ui

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.photoapp.R
import com.example.photoapp.com.example.photoapp.data.ImageItem
import com.example.photoapp.com.example.photoapp.ui.adapter.ImageListAdapter
import com.example.photoapp.databinding.ActivityLauncherBinding
import com.example.photoapp.shared.Utility

/**
 * Created by Utsav Shrestha on on 04/06/24.
 * Copyright (c) Utsav Shrestha. All rights reserved.
 * utsavshrestha102@gmail.com
 **/

class LauncherActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var binding: ActivityLauncherBinding
    private val permissions = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
        arrayOf(
            Manifest.permission.READ_MEDIA_VIDEO,
            Manifest.permission.READ_MEDIA_IMAGES
        )
    } else {
        arrayOf(
            Manifest.permission.READ_EXTERNAL_STORAGE
        )
    }
    private var imageUris: ArrayList<Uri> = ArrayList()
    private lateinit var imageListAdapter: ImageListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLauncherBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initListeners()
    }

    override fun onResume() {
        super.onResume()
        if (!Utility.checkPermissions(this, permissions)) {
            ActivityCompat.requestPermissions(
                this,
                permissions,
                MY_PERMISSIONS_REQUEST_ACCOUNTS
            )
        }
    }

    private fun initListeners() {
        binding.clickToUploadFile.setOnClickListener(this)
        binding.updateListSize.setOnClickListener(this)

        binding.etListSize.addTextChangedListener(object :
            TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(p0: Editable?) {
                if (imageUris.isNotEmpty()) {
                    binding.updateListSize.visibility = View.VISIBLE
                }
            }
        })
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.click_to_upload_file -> {
                if (Utility.checkPermissions(this, permissions)) {
                    val intent =
                        Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                    intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
                    startActivityForResult(intent, PICK_IMAGES_REQUEST_CODE)
                } else {
                    ActivityCompat.requestPermissions(
                        this,
                        permissions,
                        MY_PERMISSIONS_REQUEST_ACCOUNTS
                    )
                }
            }

            R.id.update_list_size -> {
                createImageList()
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    @Deprecated("")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PICK_IMAGES_REQUEST_CODE && resultCode == Activity.RESULT_OK && data != null) {
            imageUris.clear()
            Utility.hideKeyboard(this, binding.etListSize)
            if (data.clipData != null) {
                val count = data.clipData!!.itemCount
                if (count == 2) {
                    for (i in 0 until count) {
                        val imageUri = data.clipData!!.getItemAt(i).uri
                        imageUris.add(imageUri)
                        if (imageUris.size == 2) break
                    }
                    createImageList()
                } else {
                    Toast.makeText(
                        this,
                        getString(R.string.please_select_2_images), Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }
    }

    private fun createImageList() {
        val size = binding.etListSize.text.toString().toIntOrNull() ?: 50
        val imageList = ArrayList<ImageItem>()

        for (i in 1..size) {
            val imageUri = if (isTriangular(i)) imageUris[0] else imageUris[1]
            imageList.add(ImageItem(imageUri, i))
        }

        imageListAdapter = ImageListAdapter(imageList)
        binding.recyclerView.layoutManager = LinearLayoutManager(this)
        binding.recyclerView.adapter = imageListAdapter

        binding.updateListSize.visibility = View.GONE
        Utility.hideKeyboard(this, binding.etListSize)
    }

    private fun isTriangular(n: Int): Boolean {
        var i = 1
        var triangularNumber = 1
        while (triangularNumber < n) {
            i++
            triangularNumber = i * (i + 1) / 2
        }
        return triangularNumber == n
    }

    companion object {
        const val MY_PERMISSIONS_REQUEST_ACCOUNTS = 83
        const val PICK_IMAGES_REQUEST_CODE = 1
    }

}