package com.example.photoapp.com.example.photoapp.data

import android.net.Uri

data class ImageItem(val imageUri: Uri, val index: Int)